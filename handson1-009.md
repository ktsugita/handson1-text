# [Gitによる構成管理入門](/README.md) #009

* トランスポートプロトコル
* 認証ヘルパー
* Gitホスティングサービス
* CodeCommit
    * IAMユーザをSSH認証かHTTP認証して使う
    * IAMユーザをアクセスキー認証して使う
    * MFA必須のIAMユーザでSESSION_TOKENを生成して使う
* NEGITでCodeCommitをどう使うか？
* Gitあるある

----

## トランスポートプロトコル

* リモートリポジトリへのアクセスに使用するプロトコル
    * ローカル
    * file
    * https
    * ssh
    * git

### ローカル

* ホスト内やファイル共有上のリポジトリへのアクセスに使用
* 中央リポジトリの作成が簡単
* プロトコルの知識が不要
* ファイルシステムのアクセス制御を利用する
* ファイル共有上に中央リポジトリを作成して使うのは大丈夫
* ファイル共有上に作業リポジトリを作成して使うのは要検証
    * 同時に一人しか触らないならたぶん大丈夫
* サンプル
    ```
    git clone /opt/git/redmine.git
    git clone Y:/20_BA情報システム部/30_ITインフラ・システム管理T/90_個人ワーク/git-training/handson2c.git
    git clone //sol/dfs/情報/20_BA情報システム部/30_ITインフラ・システム管理T/90_個人ワーク/git-training/handson2c.git
    ```

### file

* ホスト内やファイル共有上のリポジトリへのアクセスに使用
* アクセスできる範囲はローカルとほぼ同じだが通信用の別プロセスを介するため遅くなる
* サンプル
    ```
    git clone file:///opt/git/redmine.git
    ```

### https

* HTTPSでアクセス可能な他ホスト上のリポジトリへのアクセスに使用
* 匿名認証もBASIC認証も対応できるためよく使われる
* アクセスするたびに認証を求められるが認証ヘルパーを使えば入力を減らせる
* サンプル
    ```
    git clone https://github.com/ktsugita/handson1-text.git
    git clone https://git-codecommit.ap-northeast-1.amazonaws.com/v1/repos/etckeeper
    ```

### ssh

* SSHでアクセス可能な他ホスト上のリポジトリへのアクセスに使用
* リポジトリ利用者がリポジトリを保持するサーバにSSHアクセスできる場合はSSH認証にてアクセス制御する
* WindowsではGit for Windows同梱のSSHを使うのが簡単
* ssh-agentを使って秘密鍵をキャッシュすればパスフレーズの入力を減らせる
* サーバにアカウントが無いユーザにもSSHアクセスさせたい場合、gitユーザを作成して、gitユーザの.ssh/authorized_keysにユーザの公開鍵を登録する形でアクセス制御する
* サンプル
    ```
    git clone ubuntu08:/opt/git/redmine.git
    git clone git@github.com:ktsugita/handson1-text.git
    git clone ssh://git@server/opt/git/repo.git
    ```

### git

* 匿名・平文で通信する
* 9418/TCPを使う
* フェッチ/プル時の匿名アクセス用に用意されている
* 匿名でよければプッシュもできるが当然危険
* 最も高速だが認証できないのでいまはあまり使われていない
* サンプル
    ```
    git clone git://github.com/ktsugita/handson1-text.git
    ```

## 認証ヘルパー

* SSH認証の場合はパスフレーズを無くすかエージェントを使うことで認証情報の入力を消せる
* HTTPS認証の場合はそのままではアクセスするたびに認証情報の入力が必要になる
* 認証ヘルパーを使うとキャッシュできるので入力を消せる
* 自分で作った認証ヘルパーを使うための仕組みがある
* [認証情報の保存](https://git-scm.com/book/ja/v2/Git-のさまざまなツール-認証情報の保存)

## Gitホスティングサービス

* リポジトリ自体は簡単に作れるがアクセス制御等の管理工数がかかる
* ホスティングサービスを利用することで工数が減らせたり付随サービスが使えたりする
* 社内でホスティングサーバを動かすタイプ
    * GitLab
    * GitHub Enterprise
* 社外のサービスを利用するタイプ
    * Github.com
    * BitBucket.com
    * GitLab.com
    * CodeCommit
* [GitHub.com・BitBucket.org・GitLab.comの月額料金比較 + α](https://qiita.com/hiroponz/items/c1ed4d6c10484233bf88)

## CodeCommit

* AWSが提供するGitホスティングサービス
* バックエンドにS3を使うので安全性が高い
* AWSコンソール上でリポジトリブラウザが使えたりプルリクエストが使えたりする
* AWSの他のプロダクトと親和性が高い
* IAMユーザが必要
* HTTPSとSSHが使える
* 費用
    * [アクティブユーザ](https://aws.amazon.com/jp/codecommit/faqs/#billing)
        > **Q: AWS CodeCommit における、アクティブなユーザーの定義とは何ですか?**<br/>
        > アクティブなユーザーとは、その月に Git リクエストまたは AWS マネジメントコンソール使用して AWS CodeCommit リポジトリにアクセスするすべての一意の AWS アイデンティティ (IAM ユーザー、IAM ロール、フェデレーティッドユーザー、ルートアカウント) を指します。一意の AWS アイデンティティを使用して CodeCommit にアクセスするサーバーは、アクティブなユーザーとみなされます。
        * 1ヶ月内にAWSコンソールでリポジトリを参照するIAMユーザ
        * 1ヶ月内にGitで実アクセスがあるIAMユーザ
        * 1ヶ月内にGitで実アクセスがありインスタンスプロファイルでアクセスするEC2
    * 最初の5ユーザまで(無料)
        * 無制限のリポジトリ
        * 50GBのストレージ/月
        * 10,000回のGitリクエスト/月
    * 6ユーザ目から
        * 1 USD/月/ユーザ
        * 50GB + 10GB * ユーザ数
        * 10,000 + 2,000 * ユーザ数
    * 超過分
        * 0.06 USD/GB/月
        * 0.001 USD/Gitリクエスト

### IAMユーザをSSH認証かHTTP認証して使う

* AWSコンソールでIAMユーザで設定したHTTP認証やSSH認証が使う
* AWSCLIのCodeCommit認証ヘルパー無しでアクセスできるので多くのツールで使えてわかりやすい
* SSH認証もHTTPS認証も期限が無く利用状況もわからない
* IAMユーザにSSH公開鍵を登録
    * 生成されたユーザ名とSSH秘密鍵でアクセスする
    * `.git/config`
        ```
        [remote "origin"]
            url = ssh://git-codecommit.ap-northeast-1.amazonaws.com/v1/repos/o365-powershell
            fetch = +refs/heads/*:refs/remotes/origin/*
        ```
    * `~/.ssh/config`
        ```
        Host git-codecommit.*.amazonaws.com
            User APKXXXXXXXXXXXXXXXXXXX
            IdentityFile ~/.ssh/id_rsa
            CheckHostIP no
        ```
* 認証ヘルパーを設定する
    * `git config --get credential.helper` => 現在の設定を確認
    * `git config [--global] credential.helper wincred`
* IAMユーザで生成したHTTPS認証でアクセスする
    * `.git/config`
        ```
        [remote "origin"]
            url = https://git-codecommit.ap-northeast-1.amazonaws.com/v1/repos/o365-powershell
            fetch = +refs/heads/*:refs/remotes/origin/*
        ```

### IAMユーザをアクセスキー認証して使う

* IAMコンソールからアクセスキーとシークレットキーを発行する
* 端末にAWSCLIをインストールする
* 発行した認証情報を`codecommit`プロファイルに設定する
    * `~/.aws/credentials`
        ```
        [codecommit]
        aws_access_key_id=AKIAIOSFODNN7EXAMPLE
        aws_secret_access_key=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
        ```
    * `~/.aws/config`
        ```
        [profile codecommit]
        region=ap-northeast-1
        ```
    * 環境変数に設定してもよい
* AWSCLIのCodecommit認証ヘルパーを使う
    * `~/.git/config` or `~/.gitconfig`
        ```
        [credential]
        helper = !aws codecommit --profile codecommit credential-helper $@
        UseHttpPath = true
        ```
    * 環境変数の場合はプロファイルは不要
* HTTPS認証のみ利用可能
* GUI系ツールやIDEでは使えないことが多い
* AssumeRoleやインスタンスプロファイルでも使える
    * EC2+IAMロールはこれを使うとEC2に認証情報を埋め込まなくてよい
    * ADとIAMをSTS連携するとこれ
* アクセスキーは期限は無いが最終使用日はわかる
* MFAが必要な場合は使えない (MFAコードを入力する方法が無いため)

### MFA必須のIAMユーザでSESSION_TOKENを生成して使う

* SSH認証やHTTP認証の準備が不要だがアクセスキーは発行しておく
* GUI系ツールやIDEでは使えないことが多い
* アクセスキーも期限が無いが最終使用日はわかる
* HTTPS認証のみ利用可能
* Force_MFAポリシーがついたIAMユーザやMFA必須のAssumeRoleでも使える
* 認証情報の生成と有効期限を意識した操作が必要
* AWSCLIやPowershellで認証情報を生成して環境変数かプロファイルに保存する
    * AWS_ACCESS_KEY
    * AWS_SECRET_ACCESS_KEY
    * AWS_SESSION_TOKEN
* Gitの認証ヘルパーとawscliのcodecommit認証ヘルパーを組み合わせて使用
* 認証情報の有効期間は１時間

## NEGITでCodeCommitをどう使うか？

### CodeCommitの導入パターン

1. IAMグループにCodeCommitポリシーをつけてadmユーザを使う
    * ツールによってはCodeCommit認証ヘルパーが使えない
    * SESSION_TOKENを生成して使う方法を広く展開するのは難しそう
        * admユーザはForce_MFAポリシーを外すとアクセスキーで認証できた
        * Force_MFAポリシーを外してもすでにMFA設定している人がコンソールを使うにはMFAが必要
        * Force_MFAポリシーを外すとアクセスキー認証だけでReadOnlyAccessが得られる
        * Force_MFAポリシーを外すと新規アカウントはMFA未設定でもコンソールでReadOnlyAccessが得られる
2. IAMグループにCodeCommitポリシーをつけてCodeCommit専用IAMユーザを使う
    * admユーザは別ユーザの管理が必要だが簡単になる
    * admユーザでないユーザへの展開も同様にできる
3. ADとIAMを連携してADユーザがAssumeRoleする
    * 実際に試してないのでよくわからない
    * ツールによってはCodeCommit認証ヘルパーが使えない
    * SESSION_TOKENを生成して使う方法を広く展開するのは難しそう

### でどうする？

#### (2) がよいと思う

* CodeCommit専用IAMユーザで権限を絞り込んで使うのが汎用性がある
* IAMユーザの管理を考えるとCodeCommit用AWSアカウントを用意するのはあり
* EC2はIAMロール
* 非EC2もIAMユーザを使う
    * Git認証を埋め込む
    * IAMアクセスキーを埋め込む
    * アクセス履歴が残るのこで後者の方がよさそう

#### Github EnterpriseやGitlab EE

* IAMユーザは管理しなくてよくなる
* IssueやPullRequestやWikiを使うことでコードや設定にまつわるコミュニケーションをまとめることができる

## Gitあるある

* エディタの変更を保存しないでコミット
* インデックスに追加しないでコミット
* コミットしただけで満足してプッシュしてない
    * 個人ブランチで作業してればamendしても大丈夫
    * 個人ブランチで作業してバックアップの意味を込めて頻繁にプッシュする
    * 個人ブランチなら履歴改変しても気軽に強制プッシュできる
----
* コミットメッセージが微妙
    * コミットメッセージテンプレートを使う
    * 個人ブランチでは気にしないでマージする前に清書してもよい
    * [コミットメッセージの書き方](https://medium.com/@risacan/%E3%82%B3%E3%83%9F%E3%83%83%E3%83%88%E3%83%A1%E3%83%83%E3%82%BB%E3%83%BC%E3%82%B8%E3%81%AE%E6%9B%B8%E3%81%8D%E6%96%B9-64aeadd92057)
    * [Git のコミットメッセージをテンプレート化する](https://qiita.com/kasaharu/items/cdb587d4d27d7608b063)
    * [コミットメッセージガイドライン](https://gist.github.com/mironal/c14a2848e752baead8e2)
----
* 自分を含む誰かの変更を取り込まないでコミットしてプッシュできない
    * まめに取り込むようにする
        * 元ブランチからマージやリベースを頻繁に行うとコンフリクトを小規模に抑え込める
    * 個人ブランチで作業する
        * ブランチ期間が長いとコンフリクトする部分が増えてしまうことが多い
----
* コミットルールやブランチルールがばらばら
    * Wikiが使えるサービスだったらそこに書く
    * 使えない場合でもrulesとかguideみたいなブランチを作成してそこに書いて共有する
----
* 他メンバーやロボットが見ているいるブランチで安全確認しないで強制プッシュ
    * Git Hookで制約したり、プルリクエスト等の利用を検討する

