# [Gitによる構成管理入門](/README.md) #Appendix

* Git
    * `tig`
    * `git stash`
    * `git alias`
    * `.git/config`
    * `.gitignore`
    * 相対的なコミット名
    * シンボリックレフ
    * `git diff`
    * MacのGitで日本語ファイル名を扱う
    * 参考資料
* VSCode
    * `settings.json` の使い分け
    * おすすめ設定
    * 参考資料

----

## Git

### `tig`

* Git for Windows組み込みのテキスト版のGitクライアントだがパスが通っていない
* [Qiita - tig](https://qiita.com/tags/tig)
    * [tigでgitをもっと便利に！ addやcommitも](https://qiita.com/suino/items/b0dae7e00bd7165f79ea)
    * [Tig で Git を自由自在に操作するための .tigrc 設定例](https://qiita.com/sfus/items/063797a1dd8fdc7d032f)
* `C:\Program Files\Git\usr\bin\tig.exe`
* パスを通すかAliasを作る
    * `$Env:Path += ";C:\Program Files\Git\usr\bin"`
    * `Set-Alias -Name tig -Value (join-path ((Get-Command Git.exe).path | split-path -parent | split-path -parent) "usr\bin\tig.exe")`
* PowershellのAliasは毎回消えるので `$env:USERPROFILE\Documents\WindowsPowershell\profile.ps1` に書いておくとPowershell起動時に自動的に読み込まれる
    * [Windows PowerShell プロファイルの力](https://technet.microsoft.com/ja-jp/library/2008.10.windowspowershell.aspx?f=255&MSPPError=-2147217396)

### `git stash`

* コミットしていない変更がある状態でブランチを切り替えると切り替え先のブランチに変更を引き継いでしまう
* コミットしていない変更を一時的に退避してHEADをcleanにする
    * `git stash`
    * `git stash save <メモ>`
* 退避した変更をワークツリーに戻したりブランチにしたりできる
    * `git stash list` メモをつけておくと一覧に表示される
    * `git stash pop` ワークツリーに戻してスタッシュから削除
    * `git stash apply` ワークツリーに戻すがスタッシュからは削除しない
    * `git stash branch <branchname>` スタッシュの内容を新しいコミットにして新しいブランチを作成する
* その日作業を終えるとき未コミットの変更があればメモをつけて退避しておくことで変更を保護できるがリモートリポジトリには退避できない
* リモートリポジトリに退避したければ、スタッシュをブランチにしてプッシュする
* 例えば `u/<username>/save/<branchname>-<date>` みたいなブランチ名(命名規則はプロジェクトで確認)で保存すれば安全。不要になったら早めに削除すること

### `git alias`

* [人間らしいGitのエイリアス](https://postd.cc/human-git-aliases/)
* 多用すると他のメンバーと話が通じなくなるので注意

### `.git/config`

* リポジトリの `.git/config` はリポジトリ間で共有されない
* リポジトリ間で共有したい場合は `.gitconfig` のようなファイルをリポジトリに登録して、`.git/config` からインクルードする
    * `git config includeif."gitdir:.config".path .gitconfig`

### `.gitignore`

* [`.gitignore`の書き方](https://qiita.com/anqooqie/items/110957797b3d5280c44f)
* [`.gitignore`の使い分け](https://qiita.com/qurage/items/0333a210c151324064e8)
* 最初のコミットは親がないのでrebase等の扱いに困ることがある
    * 空白の`gitignore`だけを書いた最初のコミットを作っておくことで扱いが楽になる
* フォルダ自体にはコンテンツがないのでステージできない
    * フォルダ内に`.gitignore`を置いてステージするとコミットできる。

### 相対的なコミット名

* `master^n` 第nの親(マージコミットは複数の親コミットがある)
* `master~n` (ファーストペアレントだけを追っかけて)第nの祖先
* `master~3^2~4` 3代前の2番目の親コミットの4代前
* `master~1`の省略形は`master~`
* `master^1`の省略形は`master^`
* `master^^`は`master^1^1`と同じで`master~2`と同じだが`master^2`は別物

### シンボリックレフ

* `HEAD`
* `ORIG_HEAD`
* `FETCH_HEAD`
* `MERGE_HEAD`
* `CHERRY_PICK_HEAD`

### `git diff`

* 異なるコミットの同じファイルを比較する
    * `git diff br1 br2 -- filename`
* 異なるコミットの異なるファイルを比較する
    * `git diff br1:foo/bar.txt br2:hoge/fuga.txt`

### [MacのGitで日本語ファイル名を扱う](https://qiita.com/ttskch/items/5276f056517d191e19b5)

* `git config --global core.precomposeunicode true`
* Unicodeの正規化形式にNFCとNFDがあり、LinuxやWindowsはNFCだがMacはNFDと異なるため

### 参考資料

* [Git book](https://git-scm.com/book/ja/v2)
* [Git のコマンドだけでなく、その仕組みを学ぶ](https://www.ibm.com/developerworks/jp/devops/library/d-learn-workings-git/index.html)
* [Git 2.x シリーズの 6 つの素晴らしいフィーチャー](https://japan.blogs.atlassian.com/2015/10/cool-features-git-2-x/)
* [コンセプトから理解するGitコマンド](https://www.slideshare.net/ktateish/git-concept1)
* [こわくないGit](https://www.slideshare.net/kotas/git-15276118)
* [GitHub Gitチートシート](https://services.github.com/on-demand/downloads/ja/github-git-cheat-sheet)
* [GitHub Gitチートシート(PDF)](https://services.github.com/on-demand/downloads/ja/github-git-cheat-sheet.pdf)
* [意外と知らない？ Gitコマンド 100本ノック](https://qiita.com/ueki05/items/5c233773e3186989bfd3)
* [Gitの良さが分からない？ ちょっとそこに座れ](https://www.kaitoy.xyz/2016/10/06/git-vs-subversion/)
* [Gitの分散バージョン管理の仕組み](https://www.kaitoy.xyz/2015/12/31/git-dvc/)
* [Gitのマージを図解する](https://www.kaitoy.xyz/2015/12/28/git-merge/)
* [git push コマンドの使い方と、主要オプションまとめ](http://www-creators.com/archives/1472)

### 参考図書

#### [入門git](https://www.amazon.co.jp/dp/427406767X) 2009/8/12

[![入門git](/images/41k7xonwpdL._SX349_BO1,204,203,200_.jpg)](https://www.amazon.co.jp/dp/427406767X)

* 古いので注意
* 改訂版がGit v2に対応していたら価値あり
* 2009年に買った本があります

#### [実用Git](https://www.amazon.co.jp/dp/4873114403/) 2010/2/19

[![実用Git](/images/51B2dc%2BsJGL._SX382_BO1,204,203,200_.jpg)](https://www.amazon.co.jp/dp/4873114403/)

* 古いので注意
* 改訂版がGit v2に対応していたら価値あり
* 翻訳本なので少しわかりにくい
* 2010年に買った本があります

#### [Gitによるバージョン管理](https://www.amazon.co.jp/dp/4274068641) 2011/10/25

[![Gitによるバージョン管理](/images/51WQ7GsnOZL._SX349_BO1,204,203,200_.jpg)](https://www.amazon.co.jp/dp/4274068641)

* 古いので注意
* 改訂版がGit v2に対応していたら価値あり
* 著者にGitの日本人コミッタがいる
* わかりやすい
* 2011年に買った本があります

#### [Gitが、おもしろいほどわかる基本の使い方33](https://www.amazon.co.jp/dp/4844365010) 2015/5/26

[![Gitが、おもしろいほどわかる基本の使い方33](/images/51eISWq1VUL._SX369_BO1,204,203,200_.jpg)](https://www.amazon.co.jp/dp/4844365010)

* 少し古い
* かなりいい感じだった

#### [エンジニアのためのGITの教科書](https://www.amazon.co.jp/dp/4798143669) 2016/1/20

[![エンジニアのためのGITの教科書](/images/51N%2Br5duQWL._SX375_BO1,204,203,200_.jpg)](https://www.amazon.co.jp/dp/4798143669)

* 悪くないが少し内容が薄い気がする

#### [独習Git](https://www.amazon.co.jp/dp/4798144614/) 2016/2/26

[![独習Git](/images/51IvJi4w3iL._SX402_BO1,204,203,200_.jpg)](https://www.amazon.co.jp/dp/4798144614/)

* かなりいい感じだった

#### [わかばちゃんと学ぶ Git使い方入門](https://www.amazon.co.jp/dp/4863542178) 2017/4/21

[![わかばちゃんと学ぶ Git使い方入門](/images/51Mj8x5RpZL._SX354_BO1,204,203,200_.jpg)](https://www.amazon.co.jp/dp/4863542178)

* 好みが分かれると思う
* 少し内容が薄い気がする

#### [サルでもわかるGit入門](https://www.amazon.co.jp/dp/4295004839) 2018/9/25

[![サルでもわかるGit入門](/images/51p3n5dlAlL._SX390_BO1,204,203,200_.jpg)](https://www.amazon.co.jp/dp/4295004839)

* Webページ [サルでもわかるGit入門](https://backlog.com/ja/git-tutorial/) を元に書籍化
* まだ出版されていない

## VSCode

### `settings.json` の使い分け

* ユーザ `%APPDATA%\Code\User\settings.json`
    * プロジェクトに関わらず行いたい設定
* ワークスペース `<name>.code-workspace` の settingsセクション
    * プロジェクト固有の設定だがプロジェクト内では共有したくない(するほどでもない)設定
* ルートフォルダ `.vscode\settings.json`
    * プロジェクト固有の設定でプロジェクト内で共有したい(強制したい)設定

### おすすめ設定

* gitで追跡対象のファイルを保存してない状態でコミットしようとすると確認プロンプトを出す
    * `"git.promptToSaveFilesBeforeCommit": true`

### 参考資料

* [非公式 - Visual Studio Code Docs](https://vscode-doc-jp.github.io/docs/setup/setup-overview.html)
* [特集：Visual Studio Code早分かりガイド](http://www.atmarkit.co.jp/ait/subtop/features/dotnet/all.html#xe789b9e99b86efbc9aVisualStudioCodee697a9e58886e3818be3828ae382ace382a4e38389)
* [Visual Studio Code の git 連携機能と git コマンドについて (2018/05/23)](https://qiita.com/satokaz/items/4660ce57ca8eb456a096)
* [Visual Studio Codeで言語ごとにインデントの設定をしたい](https://ja.stackoverflow.com/questions/34014/visual-studio-code%E3%81%A7%E8%A8%80%E8%AA%9E%E3%81%94%E3%81%A8%E3%81%AB%E3%82%A4%E3%83%B3%E3%83%87%E3%83%B3%E3%83%88%E3%81%AE%E8%A8%AD%E5%AE%9A%E3%82%92%E3%81%97%E3%81%9F%E3%81%84)
* [Visual Studio Codeでタブを2スペースにする](http://albatrosary.hateblo.jp/entry/2015/12/09/163714)
