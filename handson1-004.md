# [Gitによる構成管理入門](/README.md) #004

* リベースする
* やってみよう
* cherry-pickする
* 対話的リベース
* デタッチドヘッド

----

## リベースする

* [Git のブランチ機能 - リベース](https://git-scm.com/book/ja/v2/Git-%E3%81%AE%E3%83%96%E3%83%A9%E3%83%B3%E3%83%81%E6%A9%9F%E8%83%BD-%E3%83%AA%E3%83%99%E3%83%BC%E3%82%B9)
    * ブランチ統合前
        * ![ブランチ](images/1-4_branch.png)
    * マージ: `git merge experiment`
        * 現在のブランチをmasterとしてexperimentの変更を取り込む
        * 現在のブランチ `master=C3` と指定したブランチ `experiment=C4` の祖先が同じコミット `C2` から3方向マージを行う。`C2` をベースに `(C3-C2)` と `(C4-C2)` を比較してマージコミットを作成する => `C3`と`C4`が親のコミット `C5` ができて `master` ブランチが `C5` になる
        * ![マージ](images/1-4_merge.png)
    * リベース: `git rebase master`
        * 現在のブランチをexperimentとしてmasterの後ろに付け替える
        * 現在のブランチ `experiment=C4` と指定したブランチ `master=C3` の祖先が同じコミット `C2` から `C4` までの変更を `C3` に順に適用する => `C4`と同じ意味のコミット `C4'` は `C3` の子孫のコミットになる
        * ![リベース](images/1-4_rebase.png)
* マージは３つのコミット(分岐元、現在、指定)だけから３方向マージして新しいコミットを作成する
* リベースは分岐元から現在までの各コミット差分をすべて順に指定コミットに続けて適用する

## やってみよう

### 準備

* 前回の最後
    * ![前回の最後](images/1-3_merge_all.png)
* `git checkout master`
* `git reset --hard sp1`
* `git branch -D b1b2 noff ff`
    * ![余分なブランチを削除](images/1-4_before_amend.png)
* この後、逐一コンフリクトすると面倒なのでcommon.mdがコンフリクトしないように改変する
    * `git checkout b1`
    * `git checkout master common.md`
    * `git commit --amend`
    * `git tag b1old` # 比較用のb1oldタグを打っておく
    * `git checkout b2`
    * `git checkout master common.md`
    * `git commit --amend`
    * `git tag b2old` # 比較用のb2oldタグを打っておく
        * ![リベース前](images/1-4_before_rebase.png)
    * タグはブランチとほぼ同じだがコミットにより移動しない
    * マージやリベースするときはタグを打っておくと失敗しても戻せる

### リベースする

* master <= b1 <= b2 になるようにリベースする (わざと間違えてます)
    * `git checkout b1`
    * `git rebase b2`
    * 履歴が並んでいることを確認する
        * ![間違ってリベース](images/1-4_b1_rebase_b2.png)
* master <= b2 <= b1 に並んでしまったので戻して再度リベース
    * `git reset --hard ORIG_HEAD`
    * `git checkout b2`
    * `git rebase b1`
    * 履歴が並んでいることを確認する
        * ![ただしくリベース](images/1-4_b2_rebase_b1.png)
* b2の親をb1からmasterに変更する (再度分岐した形になる)
    * `git checkout b2`
    * `git rebase --onto master b1`
        * ![分岐やり直し](images/1-4_rebase_onto.png)
* 適用過程でコンフリクトした場合
    * 中断する `git rebase --abort`
    * コンフリクトしたファイルを修正してインデックスorコミットした後 `git rebase --continue`

## cherry-pickする

特定のコミットの変更内容を現在のブランチに適用する

* b1のcommon.mdに追記して、追記した内容をb2に取り込む
    * `git checkout b1`
    * `code -w common.md`
    * `git add .`
    * `git commit`
        * ![cherry-pick前](images/1-4_cherry-pick_before.png)
    * `git checkout b2`
    * `git cherry-pick b1`
        * ![cherry-pick前](images/1-4_cherry-pick_after.png)
* cherry-pickで重複した内容はうまくmergeできる場合がある
    * `git merge --no-ff b1`
    * ![うまくマージできる](images/1-4_cherry-pick_after_merge.png)
* 複数のコミットを指定できて順番に適用される
* 適用過程でコンフリクトした場合
    * 中断する `git cherry-pick --abort`
    * コンフリクトしたファイルを修正してインデックスorコミットした後 `git cherry-pick --continue`

## 対話的リベース

* リベースを対話的に行う
    * 順番の入れ替え
    * 複数のコミットをまとめ
    * コミットを分割
    * コミットメッセージを書き換え

### やってみよう

* `git checkout b2`
* `git tag sp2` # あとで戻れるようににタグを打っておく
* `git rebase -i master`
* `git rebase --abort`
* `git rebase --continue`
* `git reset --hard sp2` # 戻ってみる

## デタッチドヘッド

* ブランチはコミットすると位置が変わるがタグは変わらない
    * タグをチェックアウトするとデタッチドヘッドになる
    * この状態でもコミットできる
    * コミットを失わないためにはブランチにする

### やってみよう

* `git checkout b1old` # タグをチェックアウトしてみる
* `git status` # detouched HEAD
* `code -w common.md` # 知らずに修正してみる
* `git add .` # インデックスしてコミット
* `git commit -m 'デタッチドヘッド'` # コミット
* `git status`
* どのブランチにもいないのでgit historyに表示されない
* `git checkout -b b3` # 現在のデタッチドヘッドをb3ブランチにしてチェックアウトする
* `git status` # b3ブランチに紐付いたヘッドになっている
* `git checkout b2`
* `git merge b3` # b2にb3の変更をマージ
* `git status`
