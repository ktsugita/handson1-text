# [Gitによる構成管理入門](/README.md)

## [001](/handson1-001.md)

* [VSCode](https://code.visualstudio.com/)のインストール
* [Git for Windows](https://gitforwindows.org/)のインストール
* VSCodeの設定
* Gitの設定
* やってみよう

## [002](/handson1-002.md)

* リポジトリの論理構造
    * ワークツリー、インデックス、リポジトリ
* Gitの操作方法
    * コマンド、GUI、VSCode
* やってみよう

## [003](/handson1-003.md)

* ブランチの使い方
* ブランチを作る
* マージする
* コミットグラフを読む
* やってみよう

## [004](/handson1-004.md)

* リベースする
* やってみよう
* cherry-pickする
* 対話的リベース
* デタッチドヘッド

## [005](/handson1-005.md)

* コンセプトから理解するGitコマンド

## [006](/handson1-006.md)
* おさらい
* リモートリポジトリとリモートブランチ
* ローカルリポジトリとリモートリポジトリの関係
* ローカルブランチとリモートブランチの関係
* やってみよう

## [007](/handson1-007.md)
* プッシュ
* フェッチとプル
* 強制プッシュしたいけどどうすればいい？
* 誰かが強制プッシュしてたらどうすればいい？
* やってみよう
    * 平行開発による同時コミットの解消
    * 歴史改変と強制プッシュ
    * 改変された歴史を受け入れる

## [008](/handson1-008.md)
* 上流ブランチ
* `git branch`
* `git push`
* `git fetch`
* `git pull`
* `git clone`
* `git remote`
* 関連するgitconfigの設定
* やってみよう

## [009](/handson1-009.md)
* トランスポートプロトコル
* 認証ヘルパー
* Gitホスティングサービス
* CodeCommit
    * IAMユーザをSSH認証かHTTP認証して使う
    * IAMユーザをアクセスキー認証して使う
    * MFA必須のIAMユーザでSESSION_TOKENを生成して使う
* NEGITでCodeCommitをどう使うか？
* Gitあるある

## [Appendix](/handson1-appendix.md)

* Git
    * `tig`
    * `git stash`
    * `git alias`
    * `.git/config`
    * `.gitignore`
    * 相対的なコミット名
    * シンボリックレフ
    * `git diff`
    * MacのGitで日本語ファイル名を扱う
    * 参考資料
    * 参考資料
* VSCode
    * `settings.json` の使い分け
    * おすすめ設定
    * 参考資料
