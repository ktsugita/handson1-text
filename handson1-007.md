# [Gitによる構成管理入門](/README.md) #007

* プッシュ
* フェッチとプル
* 強制プッシュしたいけどどうすればいい？
* 誰かが強制プッシュしてたらどうすればいい？
* やってみよう
    * 平行開発による同時コミットの解消
    * 歴史改変と強制プッシュ
    * 改変された歴史を受け入れる

----

今回は [`master`] <=> [`origin/master`] <=> [`origin`の`master`] のパターンのみを扱います。

## プッシュ

* プッシュ
    * `git push`
        * ローカルだけのオブジェクト `origin/master..master` を`origin`のオブジェクトデータベースにコピー
        * `mater`が指しているコミットに`origin/master`と「`origin`の`master`」をあわせる
    * `origin/master`と「`origin`の`master`」が異なるコミットを指している場合はエラーになる
        * 進んでいるコミットをブランチに取り込んでからプッシュする `git pull` or `git pull --rebase`
    * `master`からみて`origin/master`がFastForwardマージできない(同じでも先祖でもない)場合はエラーになる
        * プッシュ済みのコミットをリベースするとこうなる
        * 安全とわかっている場合(個人ブランチ等)は強制プッシュして大丈夫
* 強制プッシュ
    * `git push -f`
    * `git reset`や`git rebase`や`git commit --amend`等で履歴を改変すると通常のプッシュはできないので強制プッシュが必要
    * ローカルリポジトリに無いコミットが中央リポジトリにあるとそのコミットをどこからも辿れなくなる(=失う)
    * 複数人が同じ上流ブランチを元に作業している場合、直前に`git pull --rebase`を実行しておくこと
    * `git push --force-with-lease` を使うと安全と書いてある記事もあるが、絶対ではない
        * `origin/master`と「`origin`の`master`」が異なるコミットを指している場合はエラーになる
        * フェッチすると`origin/master`と「`origin`の`master`」が同じになり、チェック機構が働かなくなる

## フェッチとプル

* リモートブランチの変更を取り出してローカルに反映する作業
    * ２つの動作を連続して実施している
    * `git pull`          => `git fetch` + `git merge orign/master`
    * `git pull --rebase` => `git fetch` + `git rebase origin/master`

### フェッチ
* `git fetch`
* `git push`と対になる動作
* `origin`のオブジェクトデータベースからローカルにないオブジェクトを取得する
* 「`origin`の`master`」が指しているコミットに`origin/master`を合わせる
* デフォルトではリモートブランチすべてをフェッチする

### プル(マージ)

* `git pull`
* フェッチする `git fetch`
    * 「`origin`の`master`」が指すコミットを`origin/master`が指すように変更する
* そしてマージする `git merge origin/master`
    * FFマージできれば進めるだけ
    * FFマージできない場合はフェッチしてきた`origin/master`と`master`のマージコミットが作成されて`master`になる
* 戻したい
    * `git reset --hard ORIG_HEAD`
    * `orgin/master`は戻せない

### プル(リベース)

* `git pull --rebase`
* フェッチする `git fetch`
    * 「`origin`の`master`」が指すコミットを`origin/master`が指すように変更する
* そしてリベースする `git rebase origin/master`
    * FFマージできれば進めるだけ
    * FFマージできない場合はフェッチしてきた`origin/master`の上にリベースする
* 戻したい
    * `git reset --hard ORIG_HEAD`
    * `orgin/master`は戻せない

## 強制プッシュしたいけどどうすればいい？

* 他に誰も使っていないブランチなら気にせずプッシュ
* 誰かが使っているブランチなら、他の人に断りをいれて強制プッシュ

## 誰かが強制プッシュしてたらどうすればいい？

* リモートの状態になればよい場合
    * ローカルに追加コミットがないかあっても捨ててよい場合
    * `git fetch`
    * `git reset --hard origin/master`
* ローカルに追加のコミットがある場合
    * `git pull --rebase`

## やってみよう

### 準備

* `Y:\20_BA情報システム部\30_ITインフラ・システム管理T\90_個人ワーク\git-training\copy-repo.bat`を実行してください。このバッチファイルで下記の3行の内容を実行しています。
    * `C:\Repo`に `handson2.git`, `handson2-a`, `handson2-b` があれば削除
    * `Y:\20_BA情報システム部\30_ITインフラ・システム管理T\90_個人ワーク\git-training` の同名フォルダをコピー
    * ワークスペースファイルをコピー
* エディタでワークスペースを開いて見やすくする
    * `code C:/Repo/handson2.code-workspace`
    * 4分割画面でコミットグラフを確認しながら作業してください
    * コミットグラフの確認では４つのブランチヘッドの位置に注目してください

### 平行開発による同時コミット

* [a]と書いてある行は`handson2-a`で実行してください
* [b]と書いてある行は`handson2-b`で実行してください
* `handson2-a`でコミット
    * [a] `echo "handson2-a3.md" > handson2-a3.md`
    * [a] `git add .`
    * [a] `git commit -m 'a3'`
        * コミットグラフを確認する
        * `master`が`origin/master`より先に進む
* `handson2-a`からプッシュ
    * [a] `git show-ref origin/master; git ls-remote origin master`
        * 自分の`origin/master`と「`origin`の`master`」が同じコミットを指していることを確認
    * [a] `git push`
        * コミットグラフを確認する
    * [a] `git ls-remote origin master; git show-ref origin/master; git ls-remote origin master`
        * `master`と`origin/master`と「`origin`の`master`」が同じになっていることを確認
* `handson2-b`でリモートの変更を取り込まないでコミット
    * [b] `echo "handson2-b1.md" > handson2-b1.md`
    * [b] `git add .`
    * [b] `git commit -m 'b1'`
        * コミットグラフを確認する
        * 当然ながら`handson2-a`とは違う歴史が積まれている
* `handson2-b`からプッシュ
    * [a] `git show-ref origin/master; git ls-remote origin master`
        * 自分の`origin/master`と「`origin`の`master`」が異なるコミットを指していることを確認
    * [b] `git push` => エラーになる
* マージでリモートの変更を取り込む
    * [b] `git pull`
        * マージコミットが作成される
        * コミットグラフを確認する
        * `origin/master`が移動してマージコミットが作成されていることを確認
* 戻す
    * [b] `git reset --hard ORIG_HEAD`
        * コミットグラフを確認する
        * マージコミットが消えて`master`の位置が戻っていることを確認
        * `origin/master`の位置は戻らない
* リベースでリモートの変更を取り込む
    * [b] `git pull --rebase`
        * コミットグラフを確認する
        * マージコミットは作成されず、`a3`の上に`b1`が積まれていることを確認
* プッシュして`handson-a`に反映する
    * [b] `git push`
        * コミットグラフを確認する
    * [a] `git pull`
        * コミットグラフを確認する

### 歴史改変と強制プッシュ

* `handson2-a`で歴史改変して強制プッシュする
    * [a] `git rebase -i HEAD~3`
        * コミットの順番を変える
            * エディタが開くので、上から `b1` `a2` `a3` の順に並べ替えて、保存(Ctrl+S)して、クローズ(Ctrl+W)
        * コミットグラフを確認する
        * 指定した順番に並び替えた新しい枝が追加されていることを確認
    * [a] `git fetch`
        * 念のため誰かがコミットしてないか確認
        * コミットがあったら `git pull --rebase`
    * [a] `git push -f`
        * プッシュできるが`force update`という警告がでる
        * コミットグラフを確認する
        * `origin/master`が`master`の位置に移動するので枝が１本になることを確認

### 改変された歴史を受け入れる

* `handson2-b`にコミットを積む
    * [b] `echo "handson2-b2.md" > handson2-b2.md`
    * [b] `git add .`
    * [b] `git commit -m 'b2'`
        * コミットグラフを確認する
        * `b1`の上に`b2`が積まれていることを確認
* `handson2-a`で改変された歴史を`handson2-b`で受け取る
    * [b] `git fetch`
        * コミットグラフを確認する
        * 実際には直接プルしてもよいが参照の動きを確認するため事前にフェッチしている
        * `force update`という警告が出る
        * `origin/master`が新しくできた別の枝に移動している
    * [b] `git pull --rebase`
        * コミットグラフを確認する
        * `a3`の上に`b2`が積まれていることを確認
