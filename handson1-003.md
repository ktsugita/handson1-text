# [Gitによる構成管理入門](/README.md) #003

* ブランチの使い方
* ブランチを作る
* マージする
* コミットグラフを読む
* やってみよう

----

## ブランチの使い方

ブランチはコピーを作る機能、マージは他のブランチの変更を取り込む機能

* 本番・検証・開発
    * Prod,Test,Develのブランチを用意して、Devel=>Test=>Prodと順番に変更を反映させる
* 機能・修正毎
    * Releaseブランチと機能毎(Feature_xxx)、修正毎のブランチ(Fix_yyy)を用意してブランチ毎に機能・修正を完成させ、Releaseブランチにマージする
* バージョン・リリース毎
    * ベータバージョン用のブランチとか、各バージョンをメンテナンスリリースするためにコード変更を継続するためにリリース名やバージョン名をつけたブランチを維持する
* ホスト毎
    * ほとんど同じだがわずかに異なる設定ファイルをホスト毎に設置する場合に、ホスト毎にブランチを作成する

## ブランチを作る

* 現在のブランチを元に新しいブランチを作る
    * `git branch <branchname>`
* ブランチを切り替える
    * `git checkout <branchname>`
* 現在のブランチを元に新しいブランチを作成して、そのブランチに切り替える
    * `git checkout -b <branchname>`
* ブランチを切り替えるときはインデックスとワークツリーをクリーンにしておく
    * クリーンでない場合、ワークツリーとインデックスは変更先のブランチに引き継がれる
    * その結果、コンフリクトする場合がある => コンフリクトマーカーがついてしまいブランチを戻してもマーカーはついたまま
    * 変更を捨てて移動するか？(`git reset --hard <branch>`) 意味を理解してマージするか？
    * 余裕を持って切り替えるためにブランチ切り替え前はクリーンにしておく
    * 変更途中を仮ブランチにコミットする
        * 普通にブランチを作る `git checkout -b <newbranch>; git add .; git commit`
        * スタッシュを使う `git stash; git stash branch <newbranch>`
    * 仮ブランチは後で本来のブランチにマージ・リベースして消す

## マージする

現在のブランチ `x` と指定したブランチ `y` の祖先が同じコミット `a` から3方向マージを行う。`x` をベースに `(x-a)` と `(y-a)` を比較してマージコミットを作成する => `x`と`y`が親コミットになる

* 他のブランチを現在のブランチにマージする
    * `git merge <branchname>`
* 中断したマージを諦めて戻す
    * `git merge --abort`
* コンフリクトしたファイルを修正してインデックスorコミットした後
    * `git merge --continue`
* マージすると現在のブランチのコミットが最初の親コミットになり、指定したブランチのコミットが２番め以降の親コミットになるマージコミットができる
    * 現在のブランチは新しいマージコミットを指す
    * 指定したブランチは変化しない

## コミットグラフを読む

* [ブランチとマージ](https://git-scm.com/book/ja/v2/Git-%E3%81%AE%E3%83%96%E3%83%A9%E3%83%B3%E3%83%81%E6%A9%9F%E8%83%BD-%E3%83%96%E3%83%A9%E3%83%B3%E3%83%81%E3%81%A8%E3%83%9E%E3%83%BC%E3%82%B8%E3%81%AE%E5%9F%BA%E6%9C%AC)
    ![ブランチとマージ](images/1-3_branch_merge.png)
* C3とC4はC2から分岐
    * masterとiss53はC2の時に分岐した
        * 通常は master が先に存在するので、現在のブランチがmasterでC2の時に `git checkout -b iss53` を実行したと思われる
    * その後、masterにはC4のコミットがあった
    * iss53はC3とC5のコミットがあった
* C6はC4とC5のマージコミット
    * masterブランチのC4の位置で `git merge iss53` を実行してできたマージコミットがC6
    * C2,C4,C5の3つから3方向マージしてC6ができた
    * 現在のブランチ(master)はC4からC6に移動した

## やってみよう

### masterブランチにコミットをつくる

* ブランチ共通ファイル common.md を作成
* ブランチ固有ファイル master.md を作成
* masterにコミット
* git logで履歴を確認する

### b1ブランチをつくる

* b1を作成・チェックアウト
* ブランチ共通ファイル common.md を編集
* ブランチ固有ファイル master.md を削除
* ブランチ固有ファイル branch1.md を作成
* b1にコミット
* git logで履歴を確認する

### b2ブランチをつくる

* masterをチェックアウト
* b2を作成・チェックアウト
    * ブランチ共通ファイル common.md を編集
    * ブランチ固有ファイル branch2.md を作成
* b2にコミット
* git logで履歴を確認する
    * ![マージ前の状況](images/1-3_before_merge.png)

### ブランチを切り替えてファイルやファイルの中身がどうなるか確認する

* masterに切り替える
* b1に切り替える
* b2に切り替える

### マージしてみる

* 後で戻るためにsp1ブランチを作っておく
    * `git checkout master`
    * `git branch sp1`
* b1をmasterにマージする
    * `git merge b1`
    * コンフリクトする要素が無いのでそのままマージされる (FastForward)
    * `b1 - master` で master.md が削除されているのでマージすると master.md が削除される
    * FastForwardするとブランチヘッドが移動するだけでコミットは作成されない
    * git logで履歴を確認する
        * ![b1をFastForwardマージ](images/1-3_ffmerge_b1.png)

### コンフリクトしてみる

* b2をmasterにマージする
    * `git merge b2`
    * common.md がコンフリクトする
* コンフリクトしたので中断する
    * `git merge --abort` => b1マージ直後に戻る
* 再度b2をmasterにマージする
    * `git merge b2`
    * common.md がコンフリクトする
* コンフリクトを解消する
    * common.md を修正する
        * マージ元を選ぶ、マージ先を選ぶ、両方残す、比較する
    * `git add common.md`
    * `git commit`
    * `git merge --continue`
    * `git branch ff` # あとで比較するためにブランチを作成しておく
    * git logで履歴を確認する
        * ![FastForwardでマージ](images/1-3_ffmerge_b1_b2.png)
        * 素朴にマージするとb1をマージした情報が失われる
        * ブランチをマージしたことを記録したい場合に困る

### FastForwardしないマージ

* `git checkout -b noff sp1` # sp1が指す場所にnoffブランチを作成してチェックアウトする
* `git merge --no-ff -e b1`
    * `--no-ff` をつけないとFastForwardしてしまう
    * `-e` or `--edit` をつけないと自動生成されたコミットメッセージになる
* `git merge b2`
    * FastForwardしないと知っているので `--no-ff -e` をつけていない
* common.md を修正する
* `git add common.md`
* `git commit`
* `git merge --continue`
* git logで履歴を確認する
    * ![NonFastForwardでマージ](images/1-3_noffmerge_b1_b2.png)
    * b1とb2をマージした情報が残っている
* マージを記録するためNonFastForwardするならば、ブランチは記録できるか？
    * ブランチ自体は記録できないが、ブランチ後に空コミットを行うことで記録できる
    * `git commit --allow-empty`

### まとめてマージするとどう変わるか？

* `git checkout -b b1b2 sp1`
* `git merge --no-ff b1 b2`
    * `--no-ff` をつけないとb1だけFastForwardしてしまう
* common.md を修正する
* `git add common.md`
* `git commit`
* `git merge --continue`
* git logで履歴を確認する
    * ![まとめてnoffマージ](images/1-3_merge_b1b2.png)
    * マージコミットの親が３つある
* 最終的なコミットグラフ
    * ![最終的なコミットグラフ](images/1-3_merge_all.png)
