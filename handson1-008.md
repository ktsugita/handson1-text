# [Gitによる構成管理入門](/README.md) #008

* 上流ブランチ
* `git branch`
* `git push`
* `git fetch`
* `git pull`
* `git clone`
* `git remote`
* 関連するgitconfigの設定
* やってみよう

----

## 上流ブランチ

* [007](/handson1-007.md)では [`master`] <=> [`origin/master`] <=> [`origin`の`master`] のパターンのみだったが、
現実にはそれだけでは済まないのでリモートリポジトリを増やしたり減らしたり変更したりできる。
* さらに任意のローカルブランチと任意のリモートブランチ間で変更を授受できる。
* ただし常に指定を書くのは煩雑だったり指定をミスしたりするので省略する仕組みがある。
* カレントブランチを`master`にして`git push`を実行すると`origin`の`master`にプッシュするのはなぜか？
* 実際は`git push origin master:master`と入力するところを省略して`git push`になっている。
* `git push`は頻繁に入力するので省力化や操作ミスを減らすことができる
* 任意の作業ブランチと任意のリモートブランチの関連を`.git/config`に設定できる
* 上記の設定または設定で指定されたブランチを上流ブランチと呼ぶ
* ローカルブランチ毎に指定できる上流ブランチはひとつだけ
* 「リモート追跡ブランチ」は「リモートのブランチ」と一対一対応するので上流ブランチをリモート追跡ブランチで代替することがある

## `git branch`

* `git branch [-a] [-r]`
    * ブランチを表示 (`*`があるのがカレントブランチ)
    * `[-r]` リモート追跡ブランチ
    * `[-a]` すべてのブランチ

* `git branch [--list <localbranch>] -vv`
    * 上流ブランチ(を表すリモート追跡ブランチ)を確認する

* `git branch [<localbranch>] -u [<リモート追跡ブランチ>]`
    * 上流ブランチ(を表すリモート追跡ブランチ)を指定する
    * 実際にはプッシュで指定することが多いので忘れても大丈夫

## `git push`

* `git push [-f] [-u] [<remote> [<refspec>]]`
    * リモートリポジトリに変更を送る
* `-f`: 強制プッシュ
* `-u`: 送信元ブランチの上流ブランチとして受信先ブランチを指定する
* `<remote>`: リモートリポジトリ。`.config`に登録済の名前かURIを指定
    * 省略した場合は`origin`が指定される
    * 指定ブランチに上流ブランチがある場合はそのリモートリポジトリ
* `<refspec>`: ローカルブランチとリモートブランチを指定する
    * `<src>[:<dst>]`
    * `<src>`: 送信元(プッシュの場合はローカル)ブランチ
        * 省略した場合はカレントブランチが指定される
    * `:<dst>`: 受信先(プッシュの場合はリモート)ブランチ
        * 省略した場合は`src`と同名のブランチかつ上流ブランチが指定される
* `<refspec>`または`:<dst>`を省略したときの動作は`git config push.default`で変更できる。
    * nothing: 省略できない
    * current: 指定ブランチと同名のリモートブランチへプッシュ
    * upstream: 指定ブランチの上流ブランチに指定されたリモートブランチへプッシュ
    * simple: 指定ブランチと同名のリモートブランチにプッシュ。ただしそれが上流ブランチに指定されていること
    * mathing: ローカルにあるブランチと同名のリモートブランチの組み合わせすべてをプッシュする
* ローカルの`hoge`の上流ブランチとして`origin`の`hoge`を指定してプッシュする
    * `git push -u origin hoge:hoge`
* リモートブランチを削除する
    * `git push --delete origin hoge`
    * `git push origin :hoge`
* ローカルのリポジトリを経由して`<remote1>`から`<remote2>`にミラーする
    * `git config remote.<remote2>.push "+refs/remotes/<remote1>/*:refs/heads/*"`
        * 初回だけ必要
        * `<refspec>`を書かない場合のプッシュ時の動作を指定している
        * ミラーしたいブランチを限定する場合は`*`の代わりにブランチ名を入れる
        * ブランチが複数の場合は`.git/config`を直接編集して`push`行を増やす
    * `git remote update <remote1> <remote2>; git push <remote2>`
        * 2つのリモートリポジトリの状態を取得する
        * `<remote1>`の全ブランチ(または指定ブランチ)を`<remote2>`に送る
        * `git ls-remote remote2`でブランチの作成状況を確認できる
        * 余分に`HEAD`ブランチをコピーしてしまう場合、`git remote set-head <remote1> -d`で`refs/remotes/<remote1>/HEAD`が消える
        * 余分なブランチは`git push --delete remote2 refs/remotes/<remote1>/HEAD`のようにして削除する

## `git fetch`

* `git fetch [--prune] [<remote> [<refspec>]]`
    * リモートリポジトリから変更を受け取る
* `<remote>`: リモートリポジトリ。`.git/config`に登録済の名前かURIを指定
    * カレントブランチに上流ブランチがある場合はそのリモートリポジトリ
    * 上流ブランチが無い場合は`origin`が指定されたことになる
* `<refspec>`: 通常は指定しない
    * リモートブランチとリモート追跡ブランチの組を0個以上指定できる
    * 省略した場合は`remote`のすべてのブランチに対応するリモート追跡ブランチを作成・更新する
* `--prune`: 対応するリモートブランチが無いリモート追跡ブランチを削除する

## `git pull`

* `git pull [--rebase] [<remote> [<refspec>]]`
    * リモートリポジトリから変更を受け取ってカレントブランチに適用する
    * 受け取る処理はフェッチと同じ
* `<remote>`: リモートリポジトリ。`.git/config`に登録済の名前かURIを指定。通常は省略する
    * カレントブランチに上流ブランチがある場合はそのリモートリポジトリ
    * 上流ブランチが無い場合は`origin`が指定されたことになる
* `<refspec>`: 通常は省略する
    * カレントブランチにマージしたい`<remote>`のブランチ
    * 複数書いたら複数をマージする
    * 省略した場合はカレントブランチの上流ブランチの変更をカレントブランチに適用する
* `--rebase`: リベースで適用する。省略時はマージで適用する
* リモートリポジトリ`repo`にある`hoge`ブランチをローカルの`hoge`ブランチとしてチェックアウトして上流ブランチも設定する
    * `git fetch repo; git checkout hoge`
    * プッシュの反対はフェッチ
    * `git checkout hoge`だけでチェックアウトと上流ブランチ設定ができる理屈は調べてみてください

## `git clone`

* `git clone [<options>] <URI> [<path>]`
    * リモートリポジトリを元にローカルにリポジトリを作る
* `<URI>` : クローン元リポジトリを指すURI
    * `<パス>` ... ローカルプロトコル (fileスキームとは少し違う)
    * `<ホスト名>:<パス>` ... sshスキームとみなされる
    * `<URI>` ... file, git, ssh, httpsスキーム
    * トランスポートプロトコルの詳細は次回
* `<path>` : 作成されるローカルリポジトリを指すパス名
    * 省略した場合はカレントフォルダにURIの`basename`で作成される

## `git remote`

* `git remote [<subcommand>]`
    * リモートリポジトリを表す名前を操作する
* `git remote [-v] [show]`
    * リモートの一覧を取得する
* `git remote [-v] show <remote>`
    * 特定のリモートの詳細を取得する
* `git remote add <URI>`
    * リモートを追加する

## 関連するgitconfigの設定

* マージするときはかならずマージコミットを作成したい
    * `git merge`にデフォルトで`--no-ff`をつける
        * `git config merge.ff false`
    * `--no-ff`をデフォルトにしても`git merge --ff`とするとFastForwardマージできる
    * ただし、プルのマージまでマージコミットが作成されるのは困る
        * `git config pull.ff only`
* いつも`git pull`の代わりに`git pull --rebase`する
    * `git config pull.rebase preserve`
* 対応するリモートブランチが無いリモート追跡ブランチがあれば自動的に削除する
    * `git config fetch.prune true`

## やってみよう

### 準備

* [007](/handson1-007.md)で使用した`handson2-a`にて作業
* 今回のハンズオン内の`k-tsugita1`や`k-tsugita2`等は他の人と違う名前に差し替えてください
    * メアドのローカルパートを推奨
* `code .git/config` 変化がわかるようにエディタで開いておく
* `git status`
    * クリーンになっていること
    * プル＆プッシュして同じ状態にしておくこと
* `git branch -a` ブランチの一覧を取得
* `git checkout -b k-tsugita1 master`
    * 新しいブランチ`k-tsugita1`を`master`を元に作成してチェックアウトする
* `k-tsugita1`にコミットを積む
    ```
    echo "k-tsugita1.md" > k-tsugita1.md
    git add .
    git commit -m "k-tsugita1"
    ```
* `git checkout -b k-tsugita2 master`
    * 新しいブランチ`k-tsugita2`を`master`を元に作成してチェックアウトする
* `k-tsugita2`にコミットを積む
    ```
    echo "k-tsugita2.md" > k-tsugita2.md
    git add .
    git commit -m "k-tsugita2"
    ```
* `git branch -a`
* `git show-ref` ブランチの一覧とそのハッシュを取得

### 新ブランチをプッシュ

* `git checkout k-tsugita1`
    * カレントブランチを`k-tsugita1`にする
* `git ls-remote origin` リモートリポジトリのブランチ一覧とそのハッシュを取得
* `git push origin k-tsugita1`
* `git ls-remote origin`
    * `origin`に`k-tsugita1`があることを確認
* `git push` => 省略するとエラー
    * 上流ブランチが指定されていないので省略するとエラーになる
* `git push -u origin k-tsugita1`
    * 上流ブランチを指定
    * `.git/config`の変化を確認
* `git push` => 省略しても成功
    * 上流ブランチが指定されているので省略しても成功する
* `git ls-remote origin`
* `git show-ref`

### 第２リポジトリを追加

* `git remote`
* `git remote add repo2 Y:/20_BA情報システム部/30_ITインフラ・システム管理T/90_個人ワーク/git-training/handson2c.git`
    * リモートリポジトリ`repo2`を追加
    * `.git/config`の変化を確認
* `git remote`
* `git remote -v`
    * それぞれのリモートのURLを確認できる
    * 歴史的所以でフェッチとプルで違うURIを指定できるためそれぞれ２つ表示される

### ブランチを取り出す

* `git ls-remote repo2`
* `git fetch repo2`
    * リモート追跡ブランチが追加される
* `git branch -a`
* `git show-ref`
* `git checkout -b repo2_master repo2/master`
    * 新しいブランチ`repo2_master`を`repo2/master`を元に作成してチェックアウトする
    * `.git/config`の変化を確認
    * ローカルブランチが作成される
    * 上流ブランチが指定される
* `git branch -a`
* `git show-ref`

### 第２リポジトリにブランチをプッシュ

* `git checkout k-tsugita2`
    * カレントブランチを`k-tsugita2`にする
* `git config push.default simple`
    * `.git/config`の変化を確認
* `git ls-remote repo2`
* `git push repo2 -u k-tsugita2:k-tsugita2x`
    * `repo2`にローカルと違う名前のブランチを作成する
    * 上流ブランチを指定する
    * `.git/config`の変化を確認
* `git push` => エラー
    * 上流ブランチは指定されているが、ローカルブランチとリモートブランチで名前が違う
    * `push.default`が`simple`だと違う名前だと省略できないのでエラーになる
* `git config push.default upstream`
    * `.git/config`の変化を確認
* `git push` => 成功
    * `upstream`ならブランチ名が違っても省略できる
* `git show-ref`
* `git ls-remote repo2`

### リモートブランチを削除

* `git fetch repo2`
    * リモート追跡ブランチがたくさん落ちてくる
* `git branch -a`
* `git show-ref`
* `git push --delete repo2 k-tsugita2x`
    * リモートから自分が追加したブランチを削除する
* `git ls-remote repo2`
    * 消えてるはず
* `git show-ref`

### リモートリポジトリから消えたリモート追跡ブランチを削除

* `git branch -a`
* `git fetch --prune repo2`
    * リモートから削除されたブランチに対応するリモート追跡ブランチが削除される
* `git branch -a`
    * リモート追跡ブランチも消えてるはず
