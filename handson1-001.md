# [Gitによる構成管理入門](/README.md) #001

* [VSCode](https://code.visualstudio.com/)のインストール
* [Git for Windows](https://gitforwindows.org/)のインストール
* VSCodeの設定
* Gitの設定
* やってみよう

----

## [VSCode](https://code.visualstudio.com/)のインストール

* 2015年からMicrosoftが開発しているプログラマ向けエディタ
* マルチプラットフォーム (Win, MacOS, Linux)
* [Electron](https://electronjs.org/)を基盤に作られている
* 対象言語のExtensionを使うと補完等の様々な支援機能を使える
* 対象言語のExtensionを使うとデバッガが使える
* 統合ターミナルが使える
* 標準でGitをサポートしている
* その他にもExtensionが豊富
* Stable版の場合毎月6日前後に更新がある。更新後はアプリ再起動が必要
* Extensionはアプリ起動時に自動更新
* Extensionの追加・更新・削除時にはアプリ再起動が必要

### インストール時の注意事項

* 追加タスクの選択はすべてにチェックをつけるのがおすすめ
    * => インストール後にOS再起動をおすすめ
    * 参考:[【ゼロから！】Visual Studio Codeのインストールと使い方](https://eng-entrance.com/texteditor-vscode)

## [Git for Windows](https://gitforwindows.org/)のインストール

* Git: 2005年に初公開。作者はLinux作者で有名なLinus Torvalds
* Windows用Gitの実装は複数あるが現在の定番
* 部分的にmsysとbashがインストールされるため、CMD,Powershell,Bashから使える
* GUIもある
* [自分用 Git For Windowsのインストール手順](https://qiita.com/toshi-click/items/dcf3dd48fdc74c91b409)

### その他の有名なGitクライアント

* [GitHub Desktop](https://desktop.github.com/)
* [Sourcetree](https://www.sourcetreeapp.com/)
* [Fork](https://git-fork.com/windows)

### インストール時の注意事項

* Select Components
    * Additional Icons => お好み
    * Windows Explorer Integration => お好み
    * Git LFS => チェックをおすすめ
    * Associate .git* => チェックをおすすめ
    * Associate .sh => チェックをおすすめ
    * Use a TrueType font => チェックを外すをおすすめ
    * Check daily for => チェックをおすすめ
* Choosing the default editor => Use Visual Studio Code
* Adjusting your PATH envrionment => 真ん中がおすすめ
* Choosing HTTPS transport backend => 下がおすすめ
* Configuring the line ending => 一番上がおすすめ
* Configuring the terminal => 下がおすすめ
* Configuring extra options
    * Enable file system caching => チェックをおすすめ
    * Enable Git Credential Manager => チェックを外すをおすすめ
    * Enable symbolic links => チェックを外すをおすすめ

## VSCodeの設定

### VSCodeの基礎

* 画面構成
    * ![VS Codeの画面構成](images/1-1_vscode.gif)
    <br/>from http://www.atmarkit.co.jp/ait/articles/1507/10/news028_2.html
    * アクティビティーバー
        * サイドバー
    * エディタ
    * パネル
    * ステータスバー
* コマンドパレット `[Ctrl]+[Shift]+[P]`
* 統合ターミナル
* ようこそ画面
    * 過去に開いたフォルダ等を簡単に開ける
    * ヘルプの「ようこそ」と同じ。`[Alt]+[H] [W]`
* 編集中のファイルが保存される
    * ファイル名の前や後に `●` があるファイルやタブは未保存。
    * `[Ctrl]+[S]` で保存する
* MarkdownとMarkdownプレビュー

### Hansonで使用するため追加でインストールするExtension

* Git History (git log) # git log を見やすく表示する
* GitLens # Gitの高機能で便利なExtension
* Terminal # 統合ターミナルを開きやすくする

### その他おすすめExtension

* gitignore # gitignoreファイルの支援Extension
* Trainling Spaces # 行末の空白を見やすくする
* vscode-icons # ファイルアイコンセット
* Powershell # 言語モジュール
* Visual Studio Live Share # ペア・モブプログラミングツール

### User Settings

* ユーザ設定の保存場所
    * `%APPDATA%\Code\User\settings.json`: ユーザ毎の設定
    * `<workspacename>.code-workspace`: マルチルートワークスペース
    * `<workspacefolder>\.vscode\settings.json`: ワークスペース
* "editor.renderWhitespace": "all"
    * タブ等の空白文字を表示する
* "editor.formatOnSave": true
    * 保存時にフォーマットする
* "editor.renderControlCharacters": true
    * コントロール文字を表示する
* "files.autoGuessEncoding": true
    * 開くときに文字コードを推測する
* "files.insertFinalNewline": true
    * ファイル末尾に改行を加える
* "files.trimTrailingWhitespace": true
    * 行末の空白を削除する
* "git.promptToSaveFilesBeforeCommit": true,
    * コミット前に Git が保存していないファイルを確認する

## Gitの設定

* 3種のレベルが存在する
    * `git config --system` => `%ProgramFiles%\Git\mingw64\etc\gitconfig`
    * `git config --global` => `%USERPROFILE%\.gitconfig`
    * `git config (--local)` => `.git/config`
* `git config --show-origin -l`で設定場所と設定値を確認できる
* Windowsでは `%ProgramData%\Git\config` も使用される。これを編集するときは `-f` で指定する
* 通常は`global`か`local`に設定する
* 同じ項目は低い側が優先される
* `git config --show-origin -l`でどのファイルにどのような設定がおこなわれているかわかる

```Bash
git config --global user.name "Kazuhiro Tsugita" # コミットに使用する名前
git config --global user.email k-tsugita@bnba.jp # コミットに使用するメールアドレス
git config --global core.autocrlf true # リポジトリ上の改行コードをLFに揃える
git config --global core.safecrlf true # 改行コードが混在している場合は変換しない
git config --global core.quotepath false # パス名をそのまま表示する => 日本語パス名が読める
git config --global core.editor "code --wait" # コミット時のエディタにVSCodeを使用する
git config --global color.ui auto # gitコマンドの出力に色をつけて見やすくする
```

* プラットフォームが混じった時に改行問題に悩まされやすいので、リポジトリ上はLFに統一できるしくみがGitに実装されている。core.autocrlfは、auto,input,falseが選べるので、trueかinputにしておけばリポジトリ上はLFになる。trueにしておくとチェックアウトしたファイルと新規作成したファイルの改行コードが同じになる。
* 一部のテキストファイルの改行コードを強制したい場合、`.gitattributes`で指定する。[.gitattributesによる改行コードの変換設定](https://kiririmode.hatenablog.jp/entry/20170416/1492300735)
* テキストファイルと判定されてしまうバイナリファイルも`.gitattributes`で指定すれば回避できる

## やってみよう

### コミットしてみる

* 統合ターミナルのPowershellからリポジトリを作成する
    ```Powershell
    mkdir C:\Repo\
    git init C:\Repo\handson1
    code C:\Repo\handson1
    ```
* 何度かコミットしてみる
    * `README.md`を書き換えてコミットを何度か実行してみる
        * 書き込む
        * ステージングする
        * コミットする
* VSCodeで`[Ctrl]+[Shift]+P`を押して`git log`
