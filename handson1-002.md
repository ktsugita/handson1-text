# [Gitによる構成管理入門](/README.md) #002

* リポジトリの論理構造
    * ワークツリー、インデックス、リポジトリ
* Gitの操作方法
    * コマンド、GUI、VSCode
* やってみよう

----

## リポジトリの論理構造

* ワークツリー
    * 直接編集する普通のファイル
* インデックス
    * コミット内容を作る場所
        * 複数のファイルの変更を一つのコミットに収める
        * 一つのファイルの変更を複数のコミットに分割する
* リポジトリ
    * 管理されているファイル、コミット情報、設定等が保管されている
    * リポジトリ内ではファイル群はスナップショットで管理されている
    * Subversionは差分で管理されていた
    * 表示される差分はコマンド実行時に計算されている
    * [リポジトリの論理構造](http://archive.fabacademy.org/2016/fablabkamakura/students/56/1.html)
        ![リポジトリの論理構造](images/1-2_structure.png)

## Gitの操作方法

* VSCodeで操作する
    * ファイルの更新
    * 差分
    * ファイルの作成
    * ステージング
    * コミット
    * 履歴
* Git GUIで操作する
    * 作成や更新はVSCode行う
    * 差分
    * ステージング
    * コミット
        * コミットログは起動したVSCodeで入力
    * 履歴
* コマンドで操作する
    * CMD,Powershell,Bashから操作する
    * VSCodeの統合ターミナルからも操作できる
    * 状況確認 `git status`
    * 作成や更新はVSCode行う
    * 状況確認 `git status`
    * 差分 `git diff`
    * ステージング `git add`
    * 状況確認 `git status`
    * コミット `git commit`
        * コミットログは起動したVSCodeで入力
    * 状況確認 `git status`
    * 履歴 `git log`
* コマンドはGitのすべての操作が行える
* よく行う操作はVSCodeが簡単で十分な場合が多い
* VSCodeではやりにくい操作はコマンドで行う
* [Visual Studio Code の git 連携機能と git コマンドについて (2018/05/23)](https://qiita.com/satokaz/items/4660ce57ca8eb456a096)
* [GitHub Gitチートシート](https://services.github.com/on-demand/downloads/ja/github-git-cheat-sheet)
* [GitHub Gitチートシート(PDF)](https://services.github.com/on-demand/downloads/ja/github-git-cheat-sheet.pdf)

## やってみよう

### 何度かコミットしてみる

* 現在の状況をみる
    * `git status`
* 現在の状況のサマリをみる
    * `git status -s`
* 特定のファイルをステージング
    * `git add <fileglob>`
* カレントディレクトリ以下をステージング
    * `git add .`
* ステージング、アンステージングはVSCodeが簡単
* 一つのコミットには一つの要件だけにするのが望ましい
    * 特定のコミットの変更を取り出して他のブランチに適用することができる
    * `git cherry-pick`
* コミットする。起動したエディタにコミットメッセージを保存するとコミットされる
    * `git commit`
* メッセージを指定してコミットする
    * `git commit -m "commit message"`
* 直前のコミットをやり直す
    * `git commit --amend`
* コミットはVSCodeが簡単
* コミットログは1行目にサマリ、2行目は空けて、3行目から詳細を書くのが望ましい
    ```
    コミットサマリ(このコミットは何をしているかを短く明確に)
    (空行)
    コミット詳細(コミットを作った理由と編集内容を詳細に)
    どのファイルを触ったかはコミット情報に含まれているので
    コミットログには無くても大丈夫
    ```

### 履歴・差分を見る

* 履歴をみる
    * `git log`
* 指定個数分の履歴を見る
    * `git log -n 3` 3つ前までの履歴をみる
    * `git log -3` 3つ前までの履歴をみる
* 差分付きで履歴を見る
    * `git log -p`
* 履歴のサマリを見る
    * `git log --oneline --graph`
* 特定のファイルの履歴をみる
    * `git log <filename>`
* 最後のコミットの内容を見る
    * `git show`
* 最後のコミットとの差分を見る
    * `git diff`
* 特定のファイルの最後のコミットとの差分を見る
    * `git diff <filename>`
* 最後のコミットとインデックスの差分を見る
    * `git diff --cached`

### 戻してみる

* いろいろ編集・保存したけど、やっぱり最後にコミットした編集前のファイルに戻す
    * `git checkout <filename>`
* いろいろ編集・保存したけど、やっぱり最後にコミットした状態にワークツリーも含めて戻す
    * `git reset --hard HEAD`
* ステージングをまとめて取り消す、個別に取り消す (VSCodeからやるのが簡単)
    * `git reset HEAD [<filename>]`
* 蛇足をつけてコミットして蛇足を消してみる
    * 一つ前のコミットの状態になるように打ち消しコミットを作成する
        * `git revert HEAD`
    * さらに一つ前のコミットに戻る
        * `git reset --hard HEAD~2`
